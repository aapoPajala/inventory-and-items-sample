package com.hackygames.hackyengine.main;

import android.graphics.Color;

import com.hackygames.hackyengine.main.Item;
import com.hackygames.hackyengine.main.Game;
import com.hackygames.hackyengine.main.LevelObject;
import com.hackygames.hackyengine.main.Screen;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Inventory extends LevelObject implements Serializable {

    public static List<Inventory> inventories = new ArrayList<Inventory>();
    public Slot[] slots;
    public float x, y;
    public boolean open;

    public Inventory(int size, float xx, float yy) {
        super(xx, yy, size*150, 150);
        slots = new Slot[size];
        x = xx;
        y = yy;

        loadSlots(size, 1);

        inventories.add(this);
    }

    public Inventory(int sizeX, int sizeY, float xx, float yy) {
        super(xx, yy, sizeX*150, sizeY*150);
        slots = new Slot[sizeX*sizeY];
        x = xx;
        y = yy;

        loadSlots(sizeX, sizeY);
    }

    public void loadSlots(int x, int y) {
        for(int h = 0; h < y; h++)
            for(int w = 0; w < x; w++)
                slots[w + h * x] = new Slot(w, h);
    }

    public void draw(Screen screen) {

        for(int i = 0; i < slots.length; i++) {
            if(!slots[i].items.isEmpty() && slots[i].items.get(0).grab && Item.moving) {
                screen.setBlendColor(Color.GRAY);
                screen.drawSprite(Game.mouse.ax-slots[i].items.get(0).width/2, Game.mouse.ay-slots[i].items.get(0).height/2, slots[i].items.get(0).sprite);
                screen.disableColorBlending();
            }
            slots[i].draw(screen, x, y);
        }
    }

    public void update() {
        for(int i = 0; i < slots.length; i++){
            slots[i].update();
        }
    }

    public class Slot {

        public float maxWeight;
        public float currentWeight;
        public float x, y, ax, ay;
        public List<Item> items = new ArrayList<Item>();
        public boolean intersects;

        public Slot(float xx, float yy) {
            x = xx;
            y = yy;
            maxWeight = 10;
        }

        public boolean fits(Item i) {
            return currentWeight+i.weight <= maxWeight;
        }

        public void addItem(Item i) {

            if(fits(i)) {

                if(items.isEmpty() || i.id.equals(items.get(0).id)) {
                    items.add(i);
                    currentWeight += i.weight;
                    i.inInventory = true;
                    i.grab = false;
                    i.solid = false;
                    i.gravity = false;
                    i.delete = true;
                } else i.inInventory = false;
            }
        }

        public void update() {

            for(Item i: items){
                i.updateItem();
            }

            if(Game.mouse.lx > ax && Game.mouse.lx < ax + 160 && Game.mouse.ly > ay && Game.mouse.ly < ay + 160) {
                intersects = true;
            } else intersects = false;

            Item i = null;
            if(!items.isEmpty())i = items.get(0);

            if(Game.mouse.dragX > 32 || Game.mouse.dragX < -32 || Game.mouse.dragY > 32 || Game.mouse.dragY < -32)
            if(i != null && intersects && !Item.moving && Game.mouse.hold) {
                i.grab = true;
                Item.moving = true;
            }

            if(i != null && i.grab && !Item.moving && intersects)i.grab = false;

            if(i != null && i.grab && i.inInventory && Item.moving && !intersects) {
                Slot s = null;
                for(int j = 0; j < Inventory.this.slots.length; j++) {
                    s = Inventory.this.slots[j];

                    if(s != null && s != this && s.intersects && s.fits(i)) {
                        i.grab = false;
                        s.addItem(i);
                        items.remove(i);
                        currentWeight -= i.weight;
                    }

                }
            }

            else if(i != null && i.grab && i.inInventory && !intersects){
                drop(i);
            }


        }

        public void drop(Item i) {
            i.delete = false;
            i.inInventory = false;
            i.grab = false;
            i.x = Game.mouse.lx;
            i.y = Game.mouse.ly;
            i.solid = true;
            i.gravity = true;
            items.remove(i);
            currentWeight -= i.weight;
        }


        public void draw(Screen screen, float offx, float offy) {
            screen.setDrawColor(Color.BLACK);
            screen.drawRect(offx+x*160, offy+y*160, 160, 160, true);
            screen.setFillColor(Color.LTGRAY);
            screen.fillRect(offx+x*160, offy+y*160, 160, 160, true);

            ax = offx+x*160;
            ay = offy+y*160;

            if(!items.isEmpty()) {
                Item s = items.get(0);

                screen.drawSprite(ax, ay, s.sprite, true);
                screen.setScale(Game.scale, Game.scale);
            }

            screen.drawText(""+ items.size(), ax+146-(items.size()+"").length()*20, ay+150, Color.DKGRAY, 40, true);


        }
    }
}
