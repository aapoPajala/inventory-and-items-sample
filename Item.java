package com.hackygames.hackyengine.main;

import android.graphics.Color;

import com.hackygames.cosmonoid.Inventory;
import com.hackygames.cosmonoid.PhysicsObject;
import com.hackygames.cosmonoid.main.Game;
import com.hackygames.cosmonoid.main.Screen;
import com.hackygames.cosmonoid.main.Sprite;

import java.util.ArrayList;
import java.util.List;

public class Item extends PhysicsObject {

    public float weight;
    public boolean inInventory, grab;
    public Sprite sprite;
    public String id;
    public boolean delete;
    public Type type;

    public Item(float xx, float yy, int health,  float w, Sprite s, String ID, Type t) {
        super(xx, yy, health, w, true, true, s);
        sprite = s;
        x = xx;
        y = yy;
        weight = w;
        id = ID;
        sprite.getHandCoord();
        type = t;
    }

    public enum Type {
        RANGED, MELEE, EXPLOSIVE
    }

    @Override
    public void update() {
        updateItem();
        updateCoordinates();
    }

    public static boolean moving;

    public void updateItem() {

        if(Game.mouse.dragX > width/4 || Game.mouse.dragX < -width/4 || Game.mouse.dragY > height/4 || Game.mouse.dragY < -height/4)
        if(!moving && !inInventory && Game.mouse.hold && mouseIntersect() && !Game.mouse.tap) {
            grab = true;
            moving = true;
            solid = false;
            gravity = false;
            vx = 0;
            vy = 0;
        }

        if(grab) {
            for(Inventory i: Inventory.inventories) {


                    for(int j = 0; j < i.slots.length; j++) {
                    if(i.slots[j].intersects && !i.slots[j].items.contains(this) && !inInventory) {
                        i.slots[j].addItem(this);
                    }

                }
            }
        }
        if(!Game.mouse.hold && grab && !inInventory) {
            grab = false;
            solid = true;
            gravity = true;
        }
        if(!Game.mouse.hold){
            moving = false;
        }

        if(!inInventory && grab && Game.mouse.dragX == 0 && Game.mouse.dragY == 0)grab = false;

        if(inInventory && rotation == 0f) {
            x = Game.game.player.x;
            y = Game.game.player.y+height;
        }

    }



    @Override
    public void draw(Screen screen) {
        screen.enableColorBlending();
        if(rotation != 0)screen.setRotation(rotation);

        if(scalex != 0 || scaley != 0)
            screen.setScale(scalex, scaley);
        if(!inInventory)screen.drawSprite(x, y, sprite, true);

        if(rotation != 0)screen.setRotation(0);

            if(grab) {
                screen.setBlendColor(Color.GRAY);
                screen.drawSprite(Game.mouse.lx-width/2, Game.mouse.ly-height/2, sprite, true);
                screen.disableColorBlending();
            }

        if(scalex != 0 || scaley != 0)
            screen.setScale(Game.scale, Game.scale);

        screen.setFillColor(Color.RED);
        screen.fillCircle(x, y, 16, true);
    }


}
